import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from '../models/address.module';
import { User } from '../models/user.model';

import { UserService } from '../service/user/user.service';

@Component({
  selector: 'app-user-change-form',
  templateUrl: './user-change-form.component.html',
  styleUrls: ['./user-change-form.component.css']
})
export class UserChangeFormComponent implements OnInit {
  @Input() user: User;
  userForm: FormGroup
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
    ) {}

    ngOnInit() {
      this.userForm = this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        birthday:[''],
        streetName:[''],
        number:[''],
        addition:[''],
        bus:[''],
        city:[''],
        postalCode:[''],
        country:['']
      })
    }

    get f() { return this.userForm.controls}

    onSubmit() {
      this.submitted = true;

      this.loading = true;

      this.userService.updateUser(this.getUserFromForm())
      .pipe()
      .subscribe({
         next: () => this.router.navigate(['../list'], {relativeTo: this.route})})

    }

    getUserFromForm(): User {
      //make new address form inputs
      let address: Address = new Address(
        this.userForm.controls['streetName'].value,
        this.userForm.controls['number'].value,
        this.userForm.controls['addition'].value,
        this.userForm.controls['bus'].value,
        this.userForm.controls['city'].value,
        this.userForm.controls['postalCode'].value,
        this.userForm.controls['country'].value);

      //make new user from imputs
      let user: User = User.changeInstance(
        this.userForm.controls['firstName'].value,
        this.userForm.controls['lastName'].value,
        this.userForm.controls['birthday'].value,
        address
      )

      return user
    }

}
