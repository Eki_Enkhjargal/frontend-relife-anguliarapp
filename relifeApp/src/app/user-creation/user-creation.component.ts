import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../models/user.model';
import { UserService } from '../service/user/user.service';

@Component({
  selector: 'app-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.css']
})
export class UserCreationComponent implements OnInit {
  @Input() user: User;
  userForm: FormGroup
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
    ) {}

    ngOnInit() {
      this.userForm = this.formBuilder.group({
        username: [''],
        email:[''],
        password: ['']
      })
    }

    get f() { return this.userForm.controls}

    onSubmit() {
      this.submitted = true;

      this.loading = true;

      this.userService.saveUser(this.userForm.controls['username'].value,
      this.userForm.controls['email'].value,
      this.userForm.controls['password'].value)
      // .pipe()
      .subscribe({
         next: () => this.router.navigate(['../home'], {relativeTo: this.route})})

    }

}
