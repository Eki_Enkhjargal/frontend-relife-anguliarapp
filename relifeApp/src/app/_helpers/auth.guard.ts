import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../service/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private tokenStorageService: TokenStorageService
) {}

    //can redirect users to the login page when trying to access
    // a section of the site for logged-in users
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      const user = this.tokenStorageService.getUser()
      if (user != null) {
          return true;
      } else {
        this.router.navigate(['/login'] , {queryParams: { returnUrl: state.url}})
        return false;
      }
  }
}
