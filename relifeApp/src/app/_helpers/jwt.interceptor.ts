import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../service/user/user.service';
import { environment } from 'src/environments/environment';
import { TokenStorageService } from '../service/token-storage.service';

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private tokenService: TokenStorageService) {}

  //Intercepts HttpRequersts from the project and adds header with accesToken
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = request;
    const token = this.tokenService.getToken();
    // const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJBRU8iLCJyb2xlcyI6W10sImlzcyI6Ii9hcGkvYXV0aGVudGljYXRlIiwiZXhwIjoxNjMyODIzOTEwfQ.uJT3yyHLzQ_nHvvBjGyg7uZN88D8VE8ZsqV_dEMfoN_B9iGJos_l5mKpOSnK7PwYoPwygFu32mvV7pWv4eDecA"
    if (token != null) {
      authReq = request.clone({ headers: request.headers.set(TOKEN_HEADER_KEY, 'Bearer ' + token)})
    }
    return next.handle(authReq)
  }
}

export const authInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
]
