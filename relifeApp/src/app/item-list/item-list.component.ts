import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';
import { Add } from '../models/add.module';
import { Category } from '../models/category.module';
import { AddService } from '../service/add/add.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  itemList: Add[];
  categoryList$: Observable<Category[]>;


  constructor(private service: AddService ) { }

  ngOnInit(): void {
    console.log("Initializing item list component")
    this.fetchList();
    console.log(this.itemList)

  }

  fetchList() {
    this.service.getItemList().subscribe(data=>this.itemList=data);
  }

  search(searchTerm: string){
    this.service.getItemList(searchTerm).subscribe(data=>this.itemList=data);

  }
}
