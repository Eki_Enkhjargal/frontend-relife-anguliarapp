import { Component, EventEmitter, OnInit } from '@angular/core';
import { Add } from '../models/add.module';
import { AddService } from '../service/add/add.service';
import { EditorState } from '../models/editor-state.enum';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent  {
  created: boolean = false;
  state: EditorState = EditorState.null;
  editorState: any = EditorState;

  shortLink: string = "";
  loading: boolean = false; // Flag variable
  file: File = null; // Variable to store file
  constructor(
    private service: AddService,
   ){}

  createItem(event: Add) {
    this.service.addItem(event ).subscribe((data) => {
      this.state = EditorState.created;
      console.log(data);
      setTimeout(() => (this.state = EditorState.null), 3000);
    });

    console.log("Add created")
  }
}

