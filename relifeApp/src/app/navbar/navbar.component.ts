import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../service/token-storage.service';
import { UserService } from '../service/user/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  isLoggedIn:boolean = false;
  constructor(
    private tokenService: TokenStorageService
  ) {
    tokenService.getLoggedIn.subscribe(data => {
      if (data != null) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    })
   }

  ngOnInit(): void {
    this.refresh();
  }

  refresh() {
    if (this.tokenService.getToken() != null) {
      this.isLoggedIn = true;
    }

  }

  logout(): void {
    this.tokenService.signOut();
    this.isLoggedIn = false;
  }

}
