import { UseraddsComponent } from './useradds/useradds.component';
import { AddItemComponent } from './add-item/add-item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { UserCreationComponent } from './user-creation/user-creation.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { UseraddsdetailsComponent } from './useraddsdetails/useraddsdetails.component';
import { AuthGuard } from './_helpers/auth.guard';

export const appRoutes: Routes = [{
  path: 'home',
  component: ItemListComponent
},
{
path: 'item/:id',
component: ItemDetailsComponent
},
{
  path: 'add',
  component:AddItemComponent,
  canActivate: [AuthGuard]
  },
{
path: 'user',
component: UserComponent,
canActivate: [AuthGuard]
},
{
  path: 'useradds',
  component: UseraddsComponent,
  canActivate: [AuthGuard]
  },
  {
    path: 'useritem/:id',
    component: UseraddsdetailsComponent
  },
{
  path: '',
  redirectTo: '/home',
  pathMatch: 'full'
},
{
  path: 'register',
  component: UserCreationComponent
},
{
  path: 'login',
  component: LoginComponent
},
];
