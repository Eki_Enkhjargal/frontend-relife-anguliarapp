import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { EditorState } from '../models/editor-state.enum';
import { UserService } from '../service/user/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  user: User;

  editing: boolean = false;
  updated: boolean = false;
  editorState: any = EditorState;
  state: EditorState;

  constructor(private service: UserService, private router: Router) {
    console.log("contructor")
    // this.ngOnInit();
  }

  ngOnInit(): void {
    console.log("init")
    this.getUser();
    // console.log(this.user.username)
  }

  getUser() {
    console.log('getting user')
    this.service.getProfile().subscribe(data => {this.user = data});
  }

  toggleEditing(): void {
    if (this.state === EditorState.null) {
      this.state = EditorState.editing;
    } else {
      this.state = EditorState.null;
    }
  }

  deleteUser(): void {
    this.service.removeUser(this.user.id.toString()).subscribe(() => {
      this.state = EditorState.deleted;
      setTimeout(() => this.router.navigateByUrl("home"), 3000);
    });
  }

  updateUser(user: User): void {
    this.service.updateUser(user).subscribe(() => {
      this.getUser();
      this.state = EditorState.updated;
      setTimeout(() => (this.state = EditorState.null), 3000);
    });
  }

}
