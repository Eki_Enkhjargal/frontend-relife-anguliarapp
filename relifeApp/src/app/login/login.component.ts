import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { NavbarComponent } from '../navbar/navbar.component';
import { TokenStorageService } from '../service/token-storage.service';
import { UserService } from '../service/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup
  loading = false;
  submitted = false;
  returnUrl: string;
  loggedIn = false;
  roles: string[] = [];
  failedLogin = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private tokenStorage: TokenStorageService
    ) {}

    ngOnInit() {
      this.form = this.formBuilder.group({
        username: [''],
        password: ['']
      });
      this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      if (this.tokenStorage.getToken()) {
        this.loggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        console.log(this.tokenStorage.getUser().roles);
      }
    }

    get f() { return this.form.controls}

    onSubmit() {
      this.userService.loginUser(this.f.username.value, this.f.password.value).subscribe(
        data => {
          console.log(data.access_token)
          this.tokenStorage.saveToken(data.access_token);
          this.tokenStorage.saveUser(data)

          this.loggedIn = true;
          this.roles = this.tokenStorage.getUser().roles;
          this.router.navigateByUrl(this.returnUrl);
          // this.reloadPage();
        },
        error => {
          this.failedLogin = true;
        }
      );

      // this.submitted = true;

      // this.form.get('username')

      // this.loading = true;
      // this.userService.loginUser(this.f.username.value, this.f.password.value)
      // .pipe(first())
      // .subscribe(data => {
      //   this.router.navigate([this.returnUrl]);
      // },
      // erron => {
      //   this.loading = false
      // });
      }

      reloadPage() {
        window.location.reload();
      }
}
