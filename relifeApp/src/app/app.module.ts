import { AddItemComponent } from './add-item/add-item.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { appRoutes } from './app.routing';
import { NavbarComponent } from './navbar/navbar.component';
import { ChatComponent } from './chat/chat.component';
import { AddService } from './service/add/add.service';
import { UserCreationComponent } from './user-creation/user-creation.component';
import { LoginComponent } from './login/login.component';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ItemDetailsComponent } from './item-details/item-details.component';
import { UseraddsComponent } from './useradds/useradds.component';
import { UseraddsdetailsComponent } from './useraddsdetails/useraddsdetails.component';
import { ItemFormComponent } from './item-form/item-form.component';
import { UserService } from './service/user/user.service';
import { UserChangeFormComponent } from './user-change-form/user-change-form.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    ItemComponent,
    ItemListComponent,
    ItemDetailsComponent,
    NavbarComponent,
    ChatComponent,
    UserCreationComponent,
    LoginComponent,
    AddItemComponent,
    UseraddsComponent,
    UseraddsdetailsComponent,
    ItemFormComponent,
    UserChangeFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule, // reactive
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  //  HttpClientInMemoryWebApiModule.forRoot(InMemoryItemService)
  ],
 // providers: [AddService, InMemoryItemService],
  providers:
  [
    AddService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
