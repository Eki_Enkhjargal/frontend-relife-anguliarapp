import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Add } from '../models/add.module';
import { Category } from '../models/category.module';
import { AddService } from '../service/add/add.service';
import { UserService } from '../service/user/user.service';


@Component({
  selector: 'app-useradds',
  templateUrl: './useradds.component.html',
  styleUrls: ['./useradds.component.css']
})
export class UseraddsComponent implements OnInit {

  itemList: Add[];

  constructor (private service: AddService ,
              private router: Router,
              private userService: UserService) { }

  ngOnInit(): void {
    console.log("Initializing user item list ")
    this.fetchList();


  }

  fetchList() {
    this.service.getUserItemList().subscribe(data=>this.itemList=data);
    console.log("--------------"+ this.itemList)
  }

}
