import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UseraddsComponent } from './useradds.component';

describe('UseraddsComponent', () => {
  let component: UseraddsComponent;
  let fixture: ComponentFixture<UseraddsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UseraddsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UseraddsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
