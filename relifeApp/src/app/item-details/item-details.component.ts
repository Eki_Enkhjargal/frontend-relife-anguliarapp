import { Conversation } from './../models/conversation.module';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Add } from '../models/add.module';
import { AddService } from '../service/add/add.service';
import { ChatService } from '../service/chat/chat.service';
import { UserService } from '../service/user/user.service';
import { Message } from '../models/message.module';
import { EditorState } from '../models/editor-state.enum';
import { HttpClient } from '@angular/common/http';
import { Bid } from '../models/bid.module';
import { User } from '../models/user.model';


@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {
  editing: boolean = false;
  editorState: any = EditorState;
  state: EditorState;
  item: Add;
  id: string;
  user:User;
  conv: Conversation;
  messages: Message[]
  retrieveResonse: any;
  base64Data: any;
  retrievedImage: any;
  imageName: string;
  bidPrice: number;
  bids:Bid[];
  highestBidPrice:number;
  username:string;
  @Input() add: Add;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();
  constructor(private service: AddService , private route: ActivatedRoute,
    private router: Router,
    private chatservice: ChatService,
    private userService: UserService,
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.getItem(this.id);
    this.getBids()
    this.getuser()

  }
  getImage(imageName:string) {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/api/get/' + imageName)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
      console.log("get image : ")
  }
  toggleEditing(editing: boolean): void {
    if (this.state === EditorState.null) {
      let conversation = new Conversation(
        this.item,
        this.item.title,
        this.messages
      )
      this.conv=conversation;

      this.state = EditorState.editing;
    } else {
      this.state = EditorState.null;
    }
  }

  getItem(id: string): void {
      this.service.getItem(id).subscribe((data) => {
      this.item = data;
      this.getImage(data.photoUri)
       });
  }
  getuser(): void {
    this.userService.getProfile().subscribe((data) => {
    this.user = data;
    console.log("User ::::::::://////:"+this.user.username);

    });
   }

   getBids(){
     this.service.getAddBids(this.id).subscribe((data)=>{
       this.bids=data;
       this.highestBidPrice=this.bids[0].bidPrice;
       this.username=this.bids[0].user.username;
       console.log("from getBids", data)
     });
   }
   sendBod(){
     console.log("username:  ", this.user.username)
     if(this.bidPrice<=this.highestBidPrice){
      this.state = EditorState.error
     }
     if(this.username===this.user.username){

       this.state=EditorState.offered;
     }
       else{
      let offer= new Bid(this.item, this.bidPrice, this.user);
      this.service.saveAddBid(offer).subscribe((data)=>{
       console.log(data)
      })
      this.state = EditorState.send;
     }
     setTimeout(() => (this.state = EditorState.null), 3000);
   }


}
