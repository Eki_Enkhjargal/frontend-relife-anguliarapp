import { Conversation } from './../models/conversation.module';
import { MessageType } from './../models/message-type.module';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChatService } from '../service/chat/chat.service';
import { Add } from '../models/add.module';
import { FormGroup } from '@angular/forms';
import { Message } from '../models/message.module';
import { UserService } from '../service/user/user.service';

import { EditorState } from '../models/editor-state.enum';
import { User } from '../models/user.model';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  title = 'instant-chatting';
  sender:User;
  convList:Conversation[];
  messList:Message[];

  @Input() conversation: Conversation;
  @Input()receiver:User;
  @Input() add:Add;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();
  messageText: string;
  form: FormGroup
  id:number;
  type = MessageType.PUBLIC;
  created: boolean = false;
  state: EditorState = EditorState.null;
  editorState: any = EditorState;

  constructor(
    private chatservice: ChatService,
    private userService: UserService
   ){

  }
  ngOnInit(): void {
    console.log("Reciever: "+ this.add.title);
    this.fetchList();
  //  this.fetchListMessage()

    console.log("de list::::::::::: ")

    console.log("Conversation++++++++++++: ", this.conversation);

  }
  sendMessage()
  {
   // this.fetchList();
    let message =new Message(
      this.messageText,
      new Date(),
      this.conversation);
      console.log("*****************", message)
      this.chatservice.sendMessage(message).subscribe((data) => {
        console.log("message send ",data);
        console.log("message send ",message.conversation);
        console.log("message send ",message.message);
        console.log("message send ",message.sentDate);

         });

      console.log("meaasgeText= " ,message.message)
      console.log("Messagsaved")
     // this.conversation.messages.push(message)
      this.saveConversation(this.conversation);
      console.log("from chat componnet Conv saved")
      this.state = EditorState.send;
      setTimeout(() => (this.state = EditorState.null), 3000);

  }


  getuser(): void {
    this.userService.getUser().subscribe((data) => {
    this.sender = data;
    console.log("text message: " , this.messageText)
    console.log("User ::::::::::",this.sender.username);
    });
   }
   fetchList() {
     console.log("fetchlist read")
    this.chatservice.getConversation().subscribe((data)=>{
      this.convList=data
      this.messList
      this.convList.forEach((entry)=>{
        this.messList=entry.messages
        console.log("conversation read: ",entry.subject);
      });
    })
  }
  fetchListMessage() {
    this.chatservice.getMessage().subscribe(data=>this.messList=data)
  }
  saveConversation(conversation: Conversation){
    this.chatservice.saveConversation(this.conversation).subscribe((data) => {
      console.log("Conv: ", this.conversation.subject)
      console.log("conv: ", this.conversation.add.id) ;
      console.log("conversations send  TTTTTTTTT ",data);
       });
  }
}
