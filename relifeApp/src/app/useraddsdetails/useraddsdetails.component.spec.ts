import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UseraddsdetailsComponent } from './useraddsdetails.component';

describe('UseraddsdetailsComponent', () => {
  let component: UseraddsdetailsComponent;
  let fixture: ComponentFixture<UseraddsdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UseraddsdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UseraddsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
