import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Add } from '../models/add.module';
import { EditorState } from '../models/editor-state.enum';
import { AddService } from '../service/add/add.service';
import { UserService } from '../service/user/user.service';


@Component({
  selector: 'app-useraddsdetails',
  templateUrl: './useraddsdetails.component.html',
  styleUrls: ['./useraddsdetails.component.css']
})
export class UseraddsdetailsComponent implements OnInit {

  item: Add;
  id: string;

  editing: boolean = false;
  updated: boolean = false;
  editorState: any = EditorState;
  state: EditorState;

  retrieveResonse: any;
  base64Data: any;
  retrievedImage: any;
  imageName: string;

  constructor(private service: AddService , private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.getItem(this.id);

  }
  getItem(id: string): void {
      this.service.getItem(id).subscribe((data) => {
      this.item = data;
      this.getImage(data.photoUri)

    });
  }
  getImage(imageName:string) {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/api/get/' + imageName)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
      console.log("get image : ")
  }
   toggleEditing(editing: boolean): void {
    if (this.state === EditorState.null) {
      this.state = EditorState.editing;
    } else {
      this.state = EditorState.null;
    }
  }

  deleteItem(id: string): void {
    this.httpClient.delete('http://localhost:8080/api/delete/'+this.item.photoUri).subscribe(() => {
      this.state = EditorState.deleted;
    });
    this.service.removeItem(id).subscribe(() => {
      this.state = EditorState.deleted;
      setTimeout(() => this.router.navigateByUrl("useradds"), 3000);
    });
  }

  updateItem(add: Add): void {
    this.service.updateItem(this.id, add).subscribe(() => {
      console.log(add)
      this.getItem(this.id);
      this.getImage(add.photoUri)
      this.state = EditorState.updated;
      setTimeout(() => (this.state = EditorState.null), 3000);
    });
  }
}
