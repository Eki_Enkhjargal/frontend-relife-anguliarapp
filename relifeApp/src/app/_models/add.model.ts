export class Add {
    id: number;
    username: string;
    title: string;
    description: string;
    category: string;
    price: number;
    photoUri: string;

    constructor(id:number, 
        username:string, 
        title: string,
        description: string,
        category: string,
        price: number,
        photoUri: string
    ) {
        this.id = id;
        this.username=username;
        this.title= title;
        this.description = description;
        this.category = category;
        this.price = price;
        this.photoUri = photoUri;
    }

    getId(): number {
        return this.id;
    }

    getUsername(): string {
        return this.username
    }

    getTitle(): string {
        return this.title
    }

    getDescription(): string {
        return this.description
    }

    getCategory(): string {
        return this.category
    }

    getPrice(): number {
        return this.price
    }

    getPhotoUri(): string {
        return this.photoUri
    }
}
