import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Add } from '../models/add.module';
import { AddService } from '../service/add/add.service';

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.css']
})
export class ItemFormComponent implements OnInit {

  @Input() add: Add;
  @Output() onSubmit: EventEmitter<Add> = new EventEmitter();
  form: FormGroup; // reactive
  title: string ;
  description: string;
  category: string;
  price: number;
  photoUri: string;
  highest_bid_price:number;
  listing_date: Date;
  status:string;
  ask_price: number
 // username:string="AEO";
  categoryList=["ART" ,"ANIMALS" , " APPAREL", "AUDIO", "BICYCLE", "BOOKS", "BUSINESS",
  "CHILDREN", "CD_DVD", "COMPUTER", "COLLECTION",
  "ELECTRONICS", "DIY", "HOBBY", "HOLIDAY", "HOME",
  "IMAGE", "IMMO", "MUSIC", "OTHER",
  "SERVICES", "SPORT", "SOFTWARE", "VIDEO", "VEHICLE" ]

  statusList=["UNLISTED", "LISTED", "SOLD", "DISPUTED", "ARCHIVED"]
  userFile:File
  public imagePath;
  imgURL: any;
  public message: string;
  constructor(  private httpClient: HttpClient
   ){}

  ngOnInit() {
    this.form = new FormGroup({
      title: new FormControl(this.add ? this.add.title : null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(this.add ? this.add.description : null, [
        Validators.required,
        Validators.minLength(3)
      ]),

      category: new FormControl(this.add ? this.add.category : null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      status: new FormControl(this.add ? this.add.status : null, [
        Validators.required,
        Validators.minLength(3)
      ]),

      price: new FormControl(this.add ? this.add.askPrice : null, [
        Validators.required,
        Validators.minLength(3)
      ]),
      photoUri: new FormControl(this.add ? this.add.photoUri : null)

  });
}
  submit(form): void {
  this.title= form.value.title;
  this.description=form.value.description;
  this.category=form.value.category;
  this.photoUri=form.value.photoUri;
  this.highest_bid_price=0;
  this.listing_date = new Date();
  this.status=form.value.status;
  this.ask_price=form.value.price;
    let add: Add = new Add(this.ask_price, this.category, this.description, this.highest_bid_price, this.listing_date,
      this.userFile.name, this.status, this.title );
    form.reset();
    console.log(add)
    console.log("Submit clicked ")
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.userFile, this.userFile.name);

    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:8080/api/upload', uploadImageData, { observe: 'response' })
      .subscribe((response) => {
        console.log("file: ", response)
        console.log("file name: ",response.body.valueOf())
        if (response.status === 200) {
          this.message = 'Image uploaded successfully';
        } else {
          this.message = 'Image not uploaded successfully';
        }
      }
      );
    this.onSubmit.emit(add);
  }

  onSelectFile(event) {
    if (event.target.files.length > 0)
    {
      const file = event.target.files[0];

      this.userFile = file;
     // this.f['profile'].setValue(file);

    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }

    var reader = new FileReader();

    this.imagePath = file;
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
   }
  }
}


