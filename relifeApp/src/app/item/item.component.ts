import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Add } from '../models/add.module';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  retrieveResonse: any;
  base64Data: any;
  retrievedImage: any;
  imageName: string;

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.getImage(this.item.photoUri)
  }
  @Input() item: Add;
  @Output() onUpdate: EventEmitter<any> = new EventEmitter();

  getImage(imageName:string) {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/api/get/' + imageName)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
      console.log("get image : ")
  }
}

