import { Conversation } from './conversation.module';
import { Add } from './add.module';
import { MessageType } from './message-type.module';


export class Message {
  message: string;
  sentDate: Date;
  conversation: Conversation;
  id?: string
  constructor( message: string, sentDate: Date, conversation: Conversation, id?: string){
    this.message=message;
    this.sentDate=sentDate;
    this.conversation=conversation;
  }
}

