export class Add {
    id: number ;
    title: string ;
    description: string;
    category: string;
    askPrice: number;
    photoUri: string;
    highestBidPrice:number;
    listingDate:Date;
    status:string;
    username: string;

    constructor( ask_price: number, category:string , description:string,
      highest_bid_price:number, listing_date:Date,
      photo_uri:string, status:string,  title:string,
       id?:number ){
      this.askPrice=ask_price;
      this.category=category;
      this.description=description;
      this.highestBidPrice=highest_bid_price;
      this.listingDate=listing_date;
      this.photoUri=photo_uri;
      this.status=status;
      this.title=title;

    }
 }
