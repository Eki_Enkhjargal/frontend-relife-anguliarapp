export enum EditorState {
  null,
  created,
  editing,
  updated,
  deleted,
  send,
  error,
  offered,
}
