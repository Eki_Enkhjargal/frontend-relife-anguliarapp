import { first } from "rxjs/operators";
import { Address } from "./address.module";

export class User {
    [x: string]: any;

    id: number;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    roles: string[];
    birthday: Date;
    joinDate: Date;
    rating: number;
    activated: boolean;
    activationToken: string; //needed?
    accessToken: string; //needed?
    address: Address;

    constructor(//id: number,
        username: string,
        firstName: string,
        lastName: string,
        email: string,
        password: string,
        birthday: Date,
        joinDate: Date,
        rating: number,
        activated: boolean,
        // activationToken: string,
        address: Address) {
            //this.id = id
            this.username = username
            this.firstName = firstName
            this.lastName = lastName
            this.email = email
            this.password = password
            this.birthday = birthday
            this.joinDate = joinDate
            this.rating = rating
            this.activated = activated
            // this.activationToken = activationToken
            this.address = address
        }

    static registerInstance(username: string, firstName: string, lastName: string, email: string, password: string, birthday: Date, address: Address): User {
        return new User(username, firstName, lastName, email, password, birthday, null, null, null, address);
    }

    static changeInstance(firstName: string, lastName: string, birthday: Date, address: Address): User {
        return new User(null, firstName, lastName, null, null, birthday, null, null, null, address);
    }

}
