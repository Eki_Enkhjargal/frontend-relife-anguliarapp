export class Address {
    streetName: string
    number: number
    addition: string
    bus: string
    postalCode: string
    city: string
    country:string

    constructor(streetname: string,
        number: number,
        addition: string,
        bus: string,
        postalCode: string,
        city: string,
        country:string) {
            this.streetName=streetname;
            this.number=number;
            this.addition=addition;
            this.bus=bus;
            this.postalCode=postalCode;
            this.city=city;
            this.country=country;
        }
}