import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



export class Category {

  id: number;
  name: string;
  constructor( name: string , id?: number) {
    this.id = id;
    this.name = name;
  }
}
