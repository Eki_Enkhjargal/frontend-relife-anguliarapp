import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Add } from './add.module';
import { User } from './user.model';



export class Bid {
  constructor(
    public  add:Add,
    public  bidPrice:number,
    public user: User,
    public id?: string){}
 }

