import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Add } from './add.module';
import { Message } from './message.module';


export class Conversation {
  constructor(
    public  add:Add,
    public subject: string,
    public messages: Message[],
    public id?: string){}
 }
