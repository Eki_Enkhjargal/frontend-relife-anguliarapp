import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { Tokens } from 'src/app/models/tokens.module';
import { User } from 'src/app/models/user.model';

// import { access } from 'fs';

const baseUrl = "http://localhost:8080/api"
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject: BehaviorSubject<User>
  public user: Observable<User>
  private baseUrl = "http://localhost:8080/api"


  constructor(private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
   }

  getAllUsersList(): Observable<User[]> {
    console.log('getAddList')
    return this.http.get<User[]>(`${baseUrl}/user/all`)
  }

  getUserById(id: number): Observable<User> {
    let params = new HttpParams();
    params = params.append('id', id.toString());
    return this.http.get<User>(`${baseUrl}/user`, {params})
  }

  getUserByUsername(username: string): Observable<User> {
    let params = new HttpParams();
    params = params.append('username', username);
    return this.http.post<User>(`${baseUrl}/user`, {params: params}, httpOptions);
  }

  // getUser(): Observable<User> {
  //   let headers = new HttpHeaders();
  //   headers = headers.append("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJBRU8iLCJyb2xlcyI6W10sImlzcyI6Ii9hcGkvYXV0aGVudGljYXRlIiwiZXhwIjoxNjMyMjEyOTIxfQ.Vt8cCve3l2wHlKD6c3mlANAg3pP0P6NcwLyYFErN227gVFrTd4OEYeRfSh3d2UApPgwR5GvtDSWW_dA3EpJsfA")
  //   return this.http.get<User>(`${baseUrl}/user`, {headers: headers})
  // }

  getProfile(): Observable<User> {
    return this.http.get<User>(`${baseUrl}/profile`)
  }

  saveUser(username: string, email: string, password: string) {
    // let params = new HttpParams()
    //   .set('username', username)
    //   .set('email', email)
    //   .set('password', password);
    // console.log(params)
    return this.http.post(`${baseUrl}/user?username=${username}&email=${email}&password=${password}`, username)
  }

  loginUser(username: string, password: string): Observable<Tokens> {
    let params = new HttpParams();
    params = params.append('username', username);
    // let user: Observable<User> = this.http.get<User>(`${baseUrl}/user`, {params: params})
    params = params.append('password', password);
    // return this.http.post<User>(`${this.baseUrl}/authenticate`, { params: params})
    return this.http.get<Tokens>(`${baseUrl}/authenticate?username=${username}&password=${password}`)
  }

  public get userValue(): User {
    return this.userSubject.value;
}
  getUser(): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/user`)
  }

  updateUser(user: User) {
    return this.http.put(`${this.baseUrl}/user`, user)
  }

  removeUser(id: string) {
    return this.http.delete(`${this.baseUrl}/user/${id}`)
  }


 /* getUser(username: string){
    return this.http.get<User>(`${this.baseUrl}/user/name/${username}`);
  }*/

}
