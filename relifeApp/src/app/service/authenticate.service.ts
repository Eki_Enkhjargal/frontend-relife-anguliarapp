import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  private baseUrl = "http://localhost:8080/api"


  constructor(private http: HttpClient) { }

  getAuth(user:User){
   this.http.get(`${this.baseUrl}/authenticate?username=${user.username}&password=${user.password}`)
  }
}
