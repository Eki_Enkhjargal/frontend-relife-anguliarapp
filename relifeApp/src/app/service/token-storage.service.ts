import { Injectable, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';

const TOKEN_KEY = 'auth-token'
const USER_KEY = 'auth-user'

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  @Output() getLoggedIn = new Subject();

  signOut() {
    window.sessionStorage.clear();
    this.getLoggedIn.next(null)
  }

  public saveToken(accessToken: string) {
    console.log('saving acces token')
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, accessToken);
    this.getLoggedIn.next(accessToken)

  }

  public getToken(): string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user) {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    return JSON.parse(sessionStorage.getItem(USER_KEY));
  }
}
