import { Conversation } from './../../models/conversation.module';
import { HttpClient } from '@angular/common/http';
import { Message } from 'src/app/models/message.module';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import { io } from 'socket.io-client';
import { Add } from 'src/app/models/add.module';
@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient) { }

  private baseUrl = "http://localhost:8080/api"

  getConversation():Observable<Conversation[]>{
    console.log("chat service conversation list", this.http.get<Conversation[]>(`${this.baseUrl}/conversation`))
    return this.http.get<Conversation[]>(`${this.baseUrl}/conversation`);
  }

  saveConversation(conv:Conversation): Observable<any> {
    return this.http.post(`${this.baseUrl}/conversation`, conv);
  }

  sendMessage(message:Message ): Observable<any> {
    console.log("from chatservice: " + message.message)
    return this.http.post(`${this.baseUrl}/message`, message);
  }
  getMessage():Observable<Message[]>{
    return this.http.get<Message[]>(`${this.baseUrl}/message`);
  }
}
