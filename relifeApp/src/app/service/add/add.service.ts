import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Add } from 'src/app/models/add.module';
import { Bid } from 'src/app/models/bid.module';
import { Category } from 'src/app/models/category.module';

@Injectable({
  providedIn: 'root'
})
export class AddService {


  private baseUrl = "http://localhost:8080/api"


  constructor(private http: HttpClient) { }

  getItemList(searchText?: string) : Observable<Add[]>{
    console.log('getAddList')
    if (!searchText)
      {
    return this.http.get<Add[]>(`${this.baseUrl}/adds`);
      }
    else {
      searchText = searchText.trim();
      const options = {params: new HttpParams().set('price', searchText)};
     return this.http.get<Add[]>(`${this.baseUrl}/adds`, options);
    }
  }
    addItem(item:Add ): Observable<any> {
      return this.http.post(`${this.baseUrl}/add`, item);
    }

    getUserItemList() : Observable<Add[]>{
         return this.http.get<Add[]>(`${this.baseUrl}/useradds`);
    }

    updateItem(id:String, item: Add) {
         return this.http.put(`${this.baseUrl}/add/${id}`, item);
    }

    getItem(id: string) {
      return this.http.get<Add>(`${this.baseUrl}/add/${id}`);

    }

    removeItem(id: string) {
        return this.http.delete(`${this.baseUrl}/add/${id}`);
    }

    getcategory(): Observable<Category[]> {
      return this.http.get<Category[]>(`${this.baseUrl}/categories`);
    }


    saveAddBid(bid:Bid): Observable<any> {
      return this.http.post(`${this.baseUrl}/add/bid`, bid);
    }

    getAddBids(id:string):Observable<Bid[]>{
      return this.http.get<Bid[]>(`${this.baseUrl}/add/bid/${id}` );
 }

}
